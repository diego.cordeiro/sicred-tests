package elements;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utils.seleniumUtils;

public class SimuladorElements extends seleniumUtils {

	public static WebDriver driver;

	@FindBy(name = "perfil")
	public List<WebElement> FORM_TIPO_PERFIL_VALUE;
	
	@FindBy(css = "[id='valorAplicar']")
	public WebElement FORM_VALOR_APLICAR_VALUE;
	
	@FindBy(css = "[id='valorAplicar-error']")
	public WebElement FORM_VALOR_APLICAR_ERRO;
	
	@FindBy(css = "[id='valorInvestir']")
	public WebElement FORM_VALOR_POUPAR_VALUE;
	
	@FindBy(css = "[id='valorInvestir-error']")
	public WebElement FORM_VALOR_POUPAR_ERRO;
	
	@FindBy(css = "[id='tempo']")
	public WebElement FORM_TEMPO_POUPAR_VALUE;
	
	@FindBy(css = "[class='btn']")
	public WebElement FORM_BOTAO_SELECT;
	
	@FindBy(css = "#formInvestimento > div:nth-child(4) > div.blocoInputs.clearfix > div.blocoFormulario.blocoMeses.blocoSelect > ul > li:nth-child(1) > a")
	public WebElement FORM_SELECT_MESES_VALUE;
	
	@FindBy(css = "#formInvestimento > div:nth-child(4) > div.blocoInputs.clearfix > div.blocoFormulario.blocoMeses.blocoSelect > ul > li:nth-child(2) > a")
	public WebElement FORM_SELECT_ANOS_VALUE;
		
	@FindBy(css = "[class='btn btnAmarelo btnSimular']")
	public WebElement FORM_BOTAO_SIMULAR;
	
	@FindBy(css = "[class='blocoResultadoSimulacao']")
	public WebElement TABELA_SIMULACAO_RESULTADO;
	
	@FindBy(css = "[class='texto']")
	public WebElement TABELA_SIMULACAO_TEXTO;
	
	@FindBy(css = "[class='valor']")
	public WebElement TABELA_SIMULACAO_VALOR;
	
	@FindBy(css = "[class='maisOpcoes']")
	public WebElement TABELA_SIMULACAO_OPCOES;
	
	@FindBy(css = "[class='btn btnAmarelo btnRefazer']")
	public WebElement TABELA_SIMULACAO_BOTAO_REFAZER;
	
	@FindBy(css = "[class='adicional']")
	public WebElement TABELA_SIMULACAO_ADICIONAL;	
}
