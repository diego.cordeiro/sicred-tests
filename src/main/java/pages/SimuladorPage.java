package pages;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import elements.SimuladorElements;
import setups.TestRule;
import utils.ReportUtils;
import utils.RestUtils;
import utils.seleniumUtils;

public class SimuladorPage extends SimuladorElements {
	String urlSistema;

	public SimuladorPage() {
		driver = TestRule.getDriver();
		PageFactory.initElements(TestRule.getDriver(), this);
	}

	public void acessaPaginaDoSimulador() {
		urlSistema = loadFromPropertiesFile("ambientes.properties", "AMBIENTE_SICRED");
		ReportUtils.logMensagem(Status.INFO, "Acessando página do simulador: " + urlSistema);
		driver.navigate().to(urlSistema);
		getLogConsoleBrowser();
		realizarCapturaDeTela();
	}
	
	public boolean selecionaTipoPerfil(String tipoPerfil){
		boolean retorno = false; 
		if (tipoPerfil.equals("fisica")) {
			FORM_TIPO_PERFIL_VALUE.get(0).click();
			retorno = true;
		} else if (tipoPerfil.equals("juridica")) {
			FORM_TIPO_PERFIL_VALUE.get(1).click();
			retorno = true;
		}
		realizarCapturaDeTela();
		return retorno;
	}
	
	public boolean preencherValorAAplicar(String valorAplicar){
		boolean retorno = false; 
		if(!valorAplicar.isEmpty()){
			FORM_VALOR_APLICAR_VALUE.clear();
			FORM_VALOR_APLICAR_VALUE.sendKeys(valorAplicar);
			moverMouseEManterSobreOElemento(FORM_VALOR_APLICAR_VALUE);
		}
		realizarCapturaDeTela();
		return retorno;
	}
	
	public boolean preencherValorAPoupar(String valorPoupar){
		boolean retorno = false; 
		if(!valorPoupar.isEmpty()){
			FORM_VALOR_POUPAR_VALUE.clear();
			FORM_VALOR_POUPAR_VALUE.sendKeys(valorPoupar);
			moverMouseEManterSobreOElemento(FORM_VALOR_POUPAR_VALUE);
		}
		realizarCapturaDeTela();
		return retorno;
	}
	
	public boolean preencherTempoAPoupar(String tempoPoupar){
		boolean retorno = false; 
		if(!tempoPoupar.isEmpty()){
			FORM_TEMPO_POUPAR_VALUE.clear();
			FORM_TEMPO_POUPAR_VALUE.sendKeys(tempoPoupar);
			moverMouseEManterSobreOElemento(FORM_TEMPO_POUPAR_VALUE);
		}
		realizarCapturaDeTela();
		return retorno;
	}
	
	public boolean selecionaTipoTempo(String tipotempo) {
		boolean retorno = false;

		FORM_BOTAO_SELECT.click();
		sleep(3000);

		if (tipotempo.equals("meses")) {
			String valorMeses = FORM_SELECT_MESES_VALUE.getText();
			if (valorMeses.trim().equalsIgnoreCase(tipotempo) && verificaExistenciaDeElementoNaTela(FORM_SELECT_MESES_VALUE, 5)) {
				FORM_SELECT_MESES_VALUE.click();
				moverMouseEManterSobreOElemento(FORM_SELECT_MESES_VALUE);
				retorno = true;
			}
		} else if (tipotempo.equals("anos")) {
			String valorAnos = FORM_SELECT_ANOS_VALUE.getText();
			if (valorAnos.trim().equalsIgnoreCase(tipotempo) && verificaExistenciaDeElementoNaTela(FORM_SELECT_MESES_VALUE, 5)) {
				FORM_SELECT_ANOS_VALUE.click();
				moverMouseEManterSobreOElemento(FORM_SELECT_ANOS_VALUE);
				retorno = true;
			}
		}
		realizarCapturaDeTela();
		return retorno;
	}
	
	public boolean clicaBotaoSimular() {
		boolean retorno = false;
		if(FORM_BOTAO_SIMULAR.isDisplayed()) {
			FORM_BOTAO_SIMULAR.click();
			moverMouseEManterSobreOElemento(FORM_BOTAO_SIMULAR);
			retorno = true;
		}
		realizarCapturaDeTela();
		return retorno;
	}
	
	public boolean validaTabelaResultadoSimulacao() {
		boolean retorno = false;
		if (verificaExistenciaDeElementoNaTela(TABELA_SIMULACAO_RESULTADO, 5)
				&& verificaExistenciaDeElementoNaTela(TABELA_SIMULACAO_TEXTO, 5)
				&& verificaExistenciaDeElementoNaTela(TABELA_SIMULACAO_VALOR, 5)
				&& verificaExistenciaDeElementoNaTela(TABELA_SIMULACAO_OPCOES, 5)
				&& verificaExistenciaDeElementoNaTela(TABELA_SIMULACAO_BOTAO_REFAZER, 5)
				&& verificaExistenciaDeElementoNaTela(TABELA_SIMULACAO_ADICIONAL, 5)) {
			moverMouseEManterSobreOElemento(TABELA_SIMULACAO_OPCOES);
			retorno = true;
		}
		realizarCapturaDeTela();
		return retorno;
	}
	
	public boolean validaMensagemOrientacaoValorMinimo() {
		boolean retorno = false;
		if (verificaExistenciaDeElementoNaTela(FORM_VALOR_APLICAR_ERRO, 5)
				&& verificaExistenciaDeElementoNaTela(FORM_VALOR_POUPAR_ERRO, 5)) {
			if (FORM_VALOR_APLICAR_ERRO.getText().trim().equals("Valor mínimo de 20.00")
					&& FORM_VALOR_POUPAR_ERRO.getText().trim().equals("Valor mínimo de 20.00")) {
				retorno = true;
			}

		}
		return retorno;
	}
	
	public void realizarCapturaDeTela() {
		ReportUtils.logMensagem(Status.INFO, "Realizando captura de tela", seleniumUtils.getScreenshotReport());
	}
	
	public boolean validaRequisicaoGet(String urlApi) throws IOException{
		acessaPaginaApi();
		return RestUtils.validaRetornoGet(urlApi);
	}
	
	public boolean validaDadosApiSimulador(String urlApi) throws JSONException, java.text.ParseException, IOException {
		boolean retorno = false;
		
		JSONObject json = new  JSONObject(RestUtils.getResponseAPI(urlApi));
		
		//valida existencia dos campos na api
		String[] camposEsperadosNoJson = { "id", "meses", "valor"};
		retorno = validaExistenciaCamposApi(null, json, camposEsperadosNoJson, true);
				
		//valida inteiro
		String id = json.get("id").toString();
		retorno = validaIntegridadeCampos("validar_inteiro", "id", id, id, null, null, true);
		
		//valida double
		JSONArray meses = json.getJSONArray("meses");
		retorno = validaTipoDadosMeses(meses);
		
		JSONArray valor = json.getJSONArray("valor");
		retorno = validaTipoDadosValor(valor);

		return retorno;
	}
	
	public boolean validaTipoDadosMeses(JSONArray meses) throws JSONException {
		Boolean retorno = null;
		ReportUtils.logMensagem(Status.INFO, "Valida se os campos do json array 'meses' são do tipo inteiro");
		for (int i = 0; i < meses.length(); i++) {
			String mes = meses.getString(i);
			retorno = validaIntegridadeCampos("validar_inteiro", "Mes" + i, mes, null, null, null, true);
			if(!retorno){
				break;
			}
		}
		return retorno;	
	}
	
	public boolean validaTipoDadosValor(JSONArray valores) throws JSONException {
		Boolean retorno = null;
		ReportUtils.logMensagem(Status.INFO, "Valida se os campos do json array 'valores' são do tipo decimal");
		for (int i = 0; i < valores.length(); i++) {
			String valor = valores.getString(i);
			retorno = validaIntegridadeCampos("validar_decimal", "valor" + i, valor, null, null, null, true);
			if (!retorno) {
				break;
			}
		}
		return retorno;
	}
	
	public boolean validaIntegridadeCampos(String tipoValidacao, String NomeCampo, String valorCampo, String valorCorreto, String formatoData, Integer linha, boolean exibeLog) {
		boolean retorno = false;
		String msgLinha = (linha == null) ? "" : "Linha (" + linha + "), ";
		String msgCampo = "campo '" + NomeCampo + "', com o valor '" + valorCampo + "', ";
		
		switch (retornaTipoValidacaoInteiro(tipoValidacao)) {
		// 1 - validar_obrigatorio
		case 1:
			if (valorCampo != null && !valorCampo.isEmpty()) {
				if (exibeLog)
					ReportUtils.logMensagem(Status.PASS, msgLinha + msgCampo + "nao esta vazio, validado com sucesso");
				retorno = true;
			} else {
				ReportUtils.logMensagem(Status.ERROR, msgLinha + msgCampo + "esta vazio");
				retorno = false;
			}
			break;

		// 2 - validar_igualdade
		case 2:
			if (valorCampo.equals(valorCorreto)) {
				if (exibeLog)
					ReportUtils.logMensagem(Status.PASS, msgLinha + msgCampo + "esta correto");
				retorno = true;
			} else {
				ReportUtils.logMensagem(Status.ERROR, msgLinha + msgCampo + "e diferente de '" + valorCorreto + "'");
				retorno = false;
			}
			break;

		// 3 - "validar_inteiro"
		case 3:
			if (valorCampo == null) {
				retorno = true;
			} else {
				if (valorCampo.matches("[0-9]*")) {
					if (exibeLog) {
						ReportUtils.logMensagem(Status.PASS, msgLinha + msgCampo + " e um valor inteiro, validado com sucesso");
					}
					retorno = true;
				} else {
					ReportUtils.logMensagem(Status.ERROR, msgLinha + msgCampo + " nao e um valor inteiro");
					retorno = false;
				}
			}
			break;

		// 4 - "validar_data"
		case 4:
			try {
				if (!(valorCampo  == null || valorCampo.isEmpty())) {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern((formatoData == null || formatoData.isEmpty()) ? "yyyy-MM-dd" : formatoData);
					@SuppressWarnings("unused")
					LocalDate localDate = LocalDate.parse(valorCampo, formatter);
					if (exibeLog) {
						ReportUtils.logMensagem(Status.PASS, msgLinha + msgCampo + "e válido");
					}
				}
				retorno = true;
			} catch (DateTimeParseException e) {
				ReportUtils.logMensagem(Status.ERROR,msgLinha + msgCampo + "e inválido");
				retorno = false;
			}
			break;

		// 5 - "validar_boolean"
		case 5:
			try {
				if (valorCampo == null || valorCampo.equalsIgnoreCase("true") || valorCampo.equalsIgnoreCase("false")) {
					if (exibeLog) {
						ReportUtils.logMensagem(Status.PASS, msgLinha + msgCampo + " e um valor Boolean, validado com sucesso");
					}
					retorno = true;
				} else {
					ReportUtils.logMensagem(Status.ERROR,msgLinha + msgCampo + " nao e um valor Boolean");
				}
			} catch (Exception e) {
				ReportUtils.logMensagem(Status.ERROR, e.toString());
				retorno = false;
			}
			break;
		
		// 6 - "validar_decimal"
		case 6:
			if (valorCampo == null) {
				retorno = true;
			} else {
				if (valorCampo.matches("[0-9]{0,10}[.]{1,1}[0-9]{0,4}") || valorCampo.equals("0")) {
					if (exibeLog)
						ReportUtils.logMensagem(Status.PASS, msgLinha + msgCampo + " e um valor decimal, validado com sucesso");
						retorno = true;
				} else {
					ReportUtils.logMensagem(Status.ERROR, msgLinha + msgCampo + " nao e um valor decimal");
					retorno = false;
				}
			}
		break;
		
		// 7 - "validar_big_decimal"
		case 7:
			try {
				BigDecimal valorBigDecimal = new BigDecimal(valorCampo);
				if (valorBigDecimal instanceof BigDecimal ) {
					if(exibeLog)ReportUtils.logMensagem(Status.PASS, msgLinha + msgCampo + " e um valor decimal, validado com sucesso");
			        retorno = true;
			    }
			} catch (Exception e) {
				ReportUtils.logMensagem(Status.ERROR, msgLinha + msgCampo + " nao e um valor decimal");
				ReportUtils.logMensagem(Status.ERROR, e.toString());
			}
		break;	

		default:
		break;
		}

		return retorno;
	}
	
	public Integer retornaTipoValidacaoInteiro(String tipoValidacao){
		Integer  tipoValidacaoInt = null;
		
		if(tipoValidacao.equalsIgnoreCase("validar_obrigatorio")){
			tipoValidacaoInt = 1; 
		}else if (tipoValidacao.equalsIgnoreCase("validar_igualdade")) {
			tipoValidacaoInt = 2;
		}else if (tipoValidacao.equalsIgnoreCase("validar_inteiro")) {
			tipoValidacaoInt = 3;
		}else if (tipoValidacao.equalsIgnoreCase("validar_data")) {
			tipoValidacaoInt = 4;
		}else if (tipoValidacao.equalsIgnoreCase("validar_boolean")) {
			tipoValidacaoInt = 5;
		}else if (tipoValidacao.equalsIgnoreCase("validar_decimal")) {
			tipoValidacaoInt = 6;
		}else if (tipoValidacao.equalsIgnoreCase("validar_big_decimal")) {
			tipoValidacaoInt = 7;
		}			
		
		return tipoValidacaoInt;
		
	}

	public boolean validaExistenciaCamposApi(JSONArray jsonArray, JSONObject jsonObject, String[] camposApi, boolean imprimeQtdCamposEsperados)throws JSONException {
		Boolean retorno = false;
		JSONObject objetoJson = (jsonArray != null) ? new JSONObject(jsonArray.getString(0)) : jsonObject;
		Integer qtdCamposEsperados =  objetoJson.length();
		Integer qtdCamposJson =  camposApi.length;
		if(imprimeQtdCamposEsperados) {
			ReportUtils.logMensagem(Status.INFO, (qtdCamposJson == qtdCamposEsperados) ? "Quantidade Campos Json: " + qtdCamposJson + " ---- Quantidade Campos Esperados: " + qtdCamposEsperados: "A Quantidade de campos existentes no json nao corresponde a quantidade esperada");
		}
		for (int i = 0; i < camposApi.length; i++) {
			retorno = (i == 0) ? objetoJson.has(camposApi[i].toString()) : (retorno == false) ? false : objetoJson.has(camposApi[i].toString());
			ReportUtils.logMensagem((retorno == false)? Status.ERROR : Status.PASS, (retorno == false) ? "O campo '" + camposApi[i] + "' nao esta presente no json utilizado, verifique!" : "Campo '" + camposApi[i] + "' validado com sucesso");
		}
		return retorno;
	}
	
	public void acessaPaginaApi() {
		urlSistema = loadFromPropertiesFile("ambientes.properties", "URL_API");
		ReportUtils.logMensagem(Status.INFO, "Acessando página do simulador: " + urlSistema);
		driver.navigate().to(urlSistema);
	}
	
	
	
	
}
