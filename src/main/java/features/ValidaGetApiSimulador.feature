# language: pt
Funcionalidade: Validar Get api simulacao

  Contexto: 
    Dado que a api solicitada retorne 200
      | urlApi                                                      |
      | http://5b847b30db24a100142dce1b.mockapi.io/api/v1/simulador |

  Cenario: Validar Get api simulacao
    Entao valido os dados e campos contidos no json
      | urlApi                                                      |
      | http://5b847b30db24a100142dce1b.mockapi.io/api/v1/simulador |
